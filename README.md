### Hi 👋, I'm <span color="green">Roop Sai Surampudi</span> :man_technologist:. A Telecommunication Systems Engineer :man_student:

I'm Electronics and Computer Enthusiast with good skills in programming :sunglasses:, specializing in developing efficient Web :snowflake: applications and enterprise Software applications. I love programming and aspires to build for good :running_man:. I like developing efficient and awesome applications and tools ⚒️. 
Apart from programming I have good experience in working with Cloud Computing :cloud: and Networking :snowflake:. 
Some of technical skills include Python :snake:, Java :coffee:, AWS, Google Cloud Platform :cloud:.

- 🔭 I’m currently working at Ericsson as a Student Intern.
- 🌱 I’m currently learning Django, Flutter.
<!-- - 👯 I’m looking to collaborate on 
     - 🤔 I’m looking for help with 
     - 😄 Pronouns: ...
     - ⚡ Fun fact: 
-->
- 💬 Ask me about anything (either a suggestion or a help). I try my best to help you out.
- 📫 How to reach me-> [roop.sai@outlook.in](mailto:roop.sai@outlook.in)

[![LinkedIn](https://img.shields.io/badge/LinkedIn-blue.svg?style=for-the-badge&logo=linkedin)](https://www.linkedin.com/in/roopsai/)
[![Facebook](https://img.shields.io/badge/facebook-blue.svg?style=for-the-badge&logo=facebook&logoColor=white)](https://www.facebook.com/roopsai.surampudi.1)

